<?php
// Time-stamp: <2020-05-20 18:02:06 daniel>    -*- Mode: php; -*-
// log.php


// Copyright (C) 2020 Daniel Mendyke
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS
// BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
// OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
// OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


// Project NS
//-----------------------------------------------------------------------------
namespace Stone;


// Wrapper around log functions
//-----------------------------------------------------------------------------
class Log {

  // Class constants
  const LimitOne = 1;
  const SecondElement = 1;

  // Instance variables
  private $filename = NULL;  // file to write into

  // Constructor
  public function __construct( $filename = "output.txt" ) {
    $this->filename = $filename;
  }  // end constructor

  // Parse the parameters to be logged
  private function parse( $parameter ) {
    $line = $parameter[ 0 ];  // format string
    foreach( array_slice( $parameter, Log::SecondElement ) as $value ) {
      if ( is_array( $value ) ) $value = json_encode( $value );  // convert to string
      $line = preg_replace( "/{}/", $value, $line, Log::LimitOne );
    };  // end foreach
    return $line;
  }  // end parse

  // Write data to the file specified in the constructor
  public function db( ) {
    if ( func_num_args() === 0 ) return;
    $line = $this->parse( func_get_args( ) );  // formating
    file_put_contents( $this->filename, $line, FILE_APPEND | LOCK_EX );
  }  // end out

}  // end class Log
