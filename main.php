<?php

require_once( "./log.php" );

$L = new Stone\Log( "output.txt" );

$L->db( "{}: {}\n", "red", 22 );
$L->db( "DB: {}\n", [ "one" => 12, "two", "three" => "four" ] );
$L->db( "{}:{}:{}\n", 'Monday', 'Tuesday', 'Wednesday' );
